# Loops, Callbacks and Iteration

## Objectives

1. Introduce Loops and iterators
2. Discuss callback functions and what they are
2. Review and practice JS fundamentals

## SWBATS

JavaScript - understand what a loop is in JS
JavaScript - write for loops
JavaScript - understand what a callback function is
JavaScript - use `forEach` to iterate over an array

## Introduction

We covered _a lot_ of JavaScript in the last two lectures.  We've just got one
more big concept to cover in basic JavaScript: _loops and iteration_

## Loops and Iterators

Sometimes, we need to do things repeatedly. If you remember back to our lecture
on functions, we discussed chores, and particularly, washing dishes. When we
was dishes, we follow these steps:

Wash the dishes:
  1. Turn on faucet
  2. Add soap to sponge
  3. Take dish out of sink
  4. Scrub dish with sponge
  5. Wash dish with running water
  6. Place dish in drying rack
  7. Repeat until sink does not have dishes to clean

Imagining this programmatically, we might start with...

```js
function washDishes() {
  //all the steps to washing dishes
}
```

Now, lets imagine steps 1 through 6 are all written functions _themselves_.  If we were washing one dish, our `washDishes()` function might look like:

```js
function washDishes() {
  turnOnFaucet()
  addSoapToSponge()

  takeDishOutOfSink()
  scrubDishWithSponge()
  washDishWithWater()
  placeDishOnRack()
}
```

This would be perfectly fine.. for one dish! If we were washing two, however...

```js
function washDishes() {
  turnOnFaucet()
  addSoapToSponge()

  takeDishOutOfSink()
  scrubDishWithSponge()
  washDishWithWater()
  placeDishOnRack()

  takeDishOutOfSink()
  scrubDishWithSponge()
  washDishWithWater()
  placeDishOnRack()
}
```

The first two steps... err.. functions only need to be done initially, but the last four would need to be written out over and over, for every dish that needs to be washed.

This is where loops come in.  

### `for` loops

Of the loops in JavaScript, the `for` loop is the most common. The `for` loop is
made up of four statements in the following structure:

```js
for ([initialization]; [condition]; [iteration]) {
  [loop body]
}
```

  * Initialization
    * Used to **initialize a variable**, typically as a _counter_
  * Condition
    * An expression evaluated before each pass through the loop. If this expression evaluates to true, the statements in the loop body are executed. If the expression evaluates to false, the loop exits. Similar to conditional statements!
  * Iteration
    * A statement executed at the end of each iteration. Typically, this will involve incrementing or decrementing a counter, bringing the loop ever closer to completion.
  * Loop body
    * Code that runs on each pass through the loop.

So, if we wanted code to loop five times, it woul look like:

```js
for(var counter = 0; counter < 5; counter++) {
  //code to loop every time
}
```

The variable, `counter`, is declared, then a conditional is set.  If `counter` is _less than_ 5, run loop.

The syntax in the last part, `counter++`, you haven't seen yet, but that's okay!
Using `++` is shorthand in JavaScript for _adding 1_. So on this loop, `counter`
starts as 0, the loops fires because 0 is less than 5, `counter` is incremented
by 1. Then... `counter` starts as 1, is still less than 5, so the loop fires,
and `counter` is incremented once again.  When `counter` is incremented to 5,
the condition returns false, the loop stops running and JavaScript continues on
line by line past the loop.

Applying this to our `washDishes()` function, lets say we had 5 dishes to wash:

```js
function washDishes() {
  turnOnFaucet()
  addSoapToSponge()

  for(var counter = 0; counter < 5; counter++) {
    takeDishOutOfSink()
    scrubDishWithSponge()
    washDishWithWater()
    placeDishOnRack()
  }
}
```

Ah! Now those last four functions will loop _5_ times. Since we don't usually
know just how many dishes we need to wash, we might spice this function up a bit
and add in an argument:

```js
function washDishes(numberOfDishes) {
  turnOnFaucet()
  addSoapToSponge()

  for(var counter = 0; counter < numberOfDishes; counter++) {
    takeDishOutOfSink()
    scrubDishWithSponge()
    washDishWithWater()
    placeDishOnRack()
  }
}
```

Now we've got something! If only we had a robot that we could give these
functions to and have them do our chores instead...

**It is important to note:** loops _need to have some sort of way to get out of them_. If the condition part
of these `for` loops doesn't evaluate to `false` at some point, the loop would
go on forever!

**Note:** Have students write their own loops for practice.  One challenge to give them:
can they write a loop that _logs_ the _counter_ variable? If done correctly,
they should see a logs of numbers, incrementing until the loop stops.

### `forEach`

Let's say we have a bunch of gifts to wrap. They all happen to be the same size
and shape, so for every gift, we need to cut a similarly sized piece of wrapping
paper, fold it up over the edges of the gift, tape it together, and add a nice
little card. Then we set the wrapped gift aside and moved onto the next gift.

In programming terms, we can think of our collection of gifts as an array and
the act of wrapping them as a function. For example:

```js
var gifts = ['teddy bear', 'drone', 'doll'];

function wrapGift(gift) {
  console.log("Wrapped " + gift);
}
```

That may be a lot.  Let's break it down.  The function `wrapGift` takes in _one_
gift at a time and logs "Wrapped " plus whatever is passed in as the argument,
which we've named `gift` here. We've also got an array of strings named `gifts`.

**Note:** Review for students: Using all that we've learned, how can we pass in _one_
value at a time from the `gifts` array into the function `wrapGift`?

Okay, so we can call specific elements in an array using _bracket notation_, like so:

```js
gifts[1]
// => 'drone'
```

And if we wanted to pass that in as an argument into a function:

```js
wrapGift(gifts[1])
// LOG: "Wrapped drone"
```

So if we wanted to call `wrapGift` on each item in the `gifts` array:

```js
wrapGift(gifts[0])
// LOG: "Wrapped teddy bear"
wrapGift(gifts[1])
// LOG: "Wrapped drone"
wrapGift(gifts[2])
// LOG: "Wrapped doll"
```

However, this isn't very efficient or extensible. It's a lot of repetitive code
to write out, and if we had more gifts we'd have to write a whole new line for
each.  

Sounds like a good use case for loops! With a loop, we can just write the repeated
action once and perform the action on every item in the collection.

We _could_ write this as something like:

```js
var gifts = ['teddy bear', 'drone', 'doll'];

function wrapGift(gift) {
  console.log("Wrapped " + gift);
}

for(var counter = 0; counter < gifts.length; counter++) {
  wrapGift(gifts[counter])
}
```

Ah, we see some concepts here that we've touched on before... `gifts.length` is
equal to the number of items in an array, in this case 3.  This loop will run
until `counter` is less than `gifts.length`. If we _added_ a gift to our array
(using `push()` of course!), we've got code that is dynamic enough that the
`for` loop will run 4 times instead of 3!

We also know that `counter` is equal to a number value, starting as 0. So, since
a variable _represents_ a value, `gifts[counter]` on the first iteration of our
loop will be equal to `gifts[0]`, and on the second, equal to `gifts[1]`, etc...

Neato! This code works fine, and is actually fairly abstracted, which is great.
_There is_, however, _another way..._

`forEach` is a _built-in_ method for arrays that allows us to loop through every
item in an array and _do_ something. Just like our last `for` loop, where we
called `wrapGift` on every loop, we can do the same with `forEach`. In fact,
we can rewrite the last bit of code using `forEach` like so:

```js
var gifts = ['teddy bear', 'drone', 'doll'];

gifts.forEach(function wrapGift(gift) {
  console.log("Wrapped " + gift);
})
```

Whoa whoa whoa whoa whoa. The entire `wrapGift` definite is getting _passed_ in as an argument for `forEach`?? Yep. You heard it here first..

#### we pass function **definitions** _into_ `forEach` as an argument!

That's right! Just as a refresher, we've seen so far that functions can be used in similar ways to variables:

```js
var myVariable = "veggie burgers"

function myLunch() {
  return "veggie burgers"
}

myVariable;
// => "veggie burgers"
myLunch();
// => "veggie burgers"
```

We've also seen that we can pass values, or variables, into functions as arguments:

```js
var myVariable = "hot dog"

function pluralize(argument) {
  return argument + "s"
}

pluralize(myVariable)
// => returns "hot dogs"
```

Well, we can pass _functions_ in as arguments, _as well_!  

In the case of `forEach`, this is what is known as a _callback_ function. For
the length of the array `gifts`, for... each... _element_ in the array, we pass
the element _into_ the function we provide.  This is not much different than how
we called `wrapGift(gifts[counter])` like in our `for` loops section. However,
we don't have to explicitly state things with `forEach`.  It knows to just go
through the array, take each item and pass it in as an argument to the function
we gave.

Let's look at a different example:

```js
var singularWords = ["hat", "hot dog", "veggie burger"]

singularWords.forEach(function pluralize(word) {
  console.log(word + "s")
})
```

Alternatively, since writing _just_ the word `pluralize` returns the definition of the function, we could write this as:

```js
var singularWords = ["hat", "hot dog", "veggie burger"]

function pluralize(word) {
  console.log(word + "s")
}

singularWords.forEach(pluralize)
```

Now... one thing to note about `forEach` is that it _doesn't return anything_.
Notice that we've modified our `pluralize` function from how it was earlier.
Instead of `return argument + "s"`, we have `console.log(word + "s")`.  If
we kept `return argument + "s"`, nothing would happen.  If you think about
it... if we were returning something in these loops, where would it go, anyway?
If you are very curious about this, try using a `return` in your callback and
switching out `forEach` with `map`.  The `map` method does the same thing as
`forEach` on arrays, but _returns an array_.  That array is made up of all the
things that were returned from the callback function.

**Note:** Have students try `forEach` out.  Since the method _doesn't_ return anything,
they should try using `console.log()` and `alert()` to get a result and see
something from each loop fire.  

## Moving Forward

There is more.. much more! But we will stop for now on the basics of JavaScript.
The best way to learn the language is to apply it, so our next step will be
focused on how to use what we've learned when writing JavaScript for websites.
Wait, what have we learned?  Well, in this lesson, we learned

  * how to write basic loops using `for`
  * how to use the built in `forEach`
  * what a callback function is
